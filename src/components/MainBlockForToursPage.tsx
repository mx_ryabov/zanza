import React, { Component } from 'react';

interface props {
    title: string;
    description: string;
}

export default class MainBlockForToursPage extends Component<props, {}> {
    
    render() {        
        return (
            <div className="tours-page__main-block">
                <h1>{this.props.title}</h1>
                <p>{this.props.description}</p>
            </div>
        )
    }
}
