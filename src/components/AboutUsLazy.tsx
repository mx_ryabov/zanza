import React, { Component } from 'react';
import AliceCarousel from 'react-alice-carousel';
import AsyncLazyImage from './AsyncLazyImage';

interface ICarouselImage {
    bigSize: string;
    smallSize: string;
    bigClass: string;
}

interface state {
    imgs: ICarouselImage[];
    isLoad: boolean;
}

export default class AboutUsLazy extends Component<{}, state> {
    state = {
        imgs: [],
        isLoad: true
    }

    async componentDidMount() {
        let imgs:string[] = ["first", "second", "third"];
        let resImgs:ICarouselImage[] = [];

        for (let img of imgs) {
            const small:string = (await import(`../assets/img/desktop/about-us-back-z3-first-thumb.png`)).default;
            const big:string = (await import(`../assets/img/desktop/about-us-back-z3-${img}.png`)).default;
            resImgs.push({
                smallSize: small,
                bigSize: big,
                bigClass: img
            });
        }

        this.setState({
            imgs: resImgs
        });
    }

    onLoadHandle = () => {
        this.setState({
            isLoad: false
        })
    }
    render() {
        return (
            <>
                <AliceCarousel 
                    mouseTrackingEnabled
                    autoPlay={true}
                    autoPlayInterval={5000}
                    buttonsDisabled={true}
                    responsive={{0: {items: 1}}}
                >
                    {this.state.imgs.map((img:ICarouselImage, ind:number) => {
                        return (
                            <AsyncLazyImage 
                                key={ind} 
                                className="landscape"
                                big_img_class_name={img.bigClass}
                                src={img.bigSize}
                            />
                        )
                    })}
                </AliceCarousel>
            </>
        )
    }
}
