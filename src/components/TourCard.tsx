import React, { Component } from 'react';
import config from "../configure";
import { Link, withRouter, RouteComponentProps } from 'react-router-dom';

interface props extends RouteComponentProps {
    title: string;
    price: number;
    description: string;
    photo: string;
    style: React.CSSProperties;
    title_hebrew: string;
    description_hebrew: string;
    lang: string;
    on_click_bookatour: string | null;
}

class TourCard extends Component<props> {
    render() {    
        const lang:string = this.props.location.pathname.split("/")[1];

        return (
            <div className="tour-card" style={this.props.style}>
                <div className="img">
                    <img src={`${config.urlServer}/uploads/${this.props.photo}`} alt=""/>
                </div>
                <p className="title">{
                    this.props.lang === "he" ? this.props.title_hebrew : this.props.title
                }</p>
                <div className="price">
                    <span>FROM</span>
                    <span className="num">${this.props.price}</span>
                </div>
                <p className="description">{
                    this.props.lang === "he" ? this.props.description_hebrew : this.props.description
                }</p>
                <div className="divider"></div>
                {
                    this.props.on_click_bookatour !== null ?
                    (<a className="title" href={this.props.on_click_bookatour}>book a tour</a>) :
                    (<Link to={`/${lang}/contact`} className="title">book a tour</Link>)
                }
            </div>
        )
    }
}

export default withRouter(TourCard);
