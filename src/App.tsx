import React, { Component } from 'react';
import Header from './components/Header';
import './assets/scss/main.scss';
import TourCard from './components/TourCard';
import Paginator from './components/Paginator';
import { AppState } from './store';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import { ThunkDispatch } from 'redux-thunk';
import { getTours, getComments, saveTestResults, submitEmail } from './store/landing/actions';
import { connect } from 'react-redux';
import { ITour, IComment } from './store/landing/types';
import CommentCard from './components/CommentCard';
import Test from './components/Test';
import ILocalization from './localization/interface';
import he from './localization/he';
import en from './localization/en';
import LazyLoad from 'react-lazyload';
import { RouteComponentProps, withRouter, Link } from 'react-router-dom';
import AboutUsLazy from './components/AboutUsLazy';



interface props extends RouteComponentProps {
	tours: ITour[];
	comments: IComment[];
	getTours: () => Promise<void>;
	getComments: (isMainPage:boolean) => Promise<void>;
	saveTestResults: (results:string) => Promise<void>;
	submitEmail: (email:string, test_results:string, cb: (res:any) => void) => Promise<void>;
	locationName: "en" | "he";
}

type state = {
	currentTourCardInd: number;
	currentCommentInd: number;
}

class App extends Component<props, state> {
	state = {
		currentTourCardInd: 0,
		currentCommentInd: 0
	}

	componentDidMount() {
		this.props.getTours();
		this.props.getComments(true);
	}

	componentWillUnmount() {}

	_switchTour(indexTour:number) {
		this.setState({currentTourCardInd: indexTour});	
	}
	_switchComment(indexComment: number) {
		this.setState({currentCommentInd: indexComment});
	}

	_renderTours() {
		const lang:any = this.props.match.params;
		return this.props.tours.map((tour:ITour, ind:number) => {
			return (
				<TourCard
					key={ind}
					title_hebrew={tour.title_hebrew}
					description_hebrew={tour.description_hebrew}
					lang={lang.lang}
					title={tour.title}
					description={tour.description}
					photo={tour.photo}
					price={tour.price}
					on_click_bookatour={tour.on_click_bookatour}
					style={{transform: `translateX(-${320*(this.state.currentTourCardInd)}px)`}}
				/>
			)
		})
	}

	_renderComments() {
		return this.props.comments.map((comment:IComment, ind:number) => {
			return (
				<CommentCard 
					key={ind} 
					title={comment.title}
					text={comment.text}
					rating={comment.rating}
					addressant={comment.addressant}
					date={comment.date.split('-').join('.')}
					style={{transform: `translate(-${this.state.currentCommentInd*100}%)`}}
				/>
			)
		})
	}

	render() {
		const lang:any = this.props.match.params;
		const locationObj:ILocalization = lang.lang === "he" ? he : en;	
		return (
			<div className={`${lang.lang === "he" ? "he" : "en"}`}>
				<Header />
				<div className="about-us">
					<AboutUsLazy />
					<a className="phone_number" href="tel:+61430049139">
						+61 430 049139
					</a>
				</div>
				<div className="types-of-tours" id="types_of_tours">
					<h1>{locationObj.mainPage.typesOfTours.title}</h1>
					<div className="divider"></div>
					<div className="desktop-img"></div>
					<p style={{display: "none"}}>{locationObj.mainPage.typesOfTours.description}</p>
					<div className="type" id="personal_customized_tours">
						<div className="description">
							<span className="num">1.</span>
							<h2>{locationObj.mainPage.typesOfTours.items[0].title}</h2>
							<p>{locationObj.mainPage.typesOfTours.items[0].description}</p>
							<Link to={`${lang.lang}/personal_customized_tours`}>More details</Link>
						</div>
						<div className="img"></div>
					</div>
					<div className="type" id="ground_services">
						<div className="description">
							<span className="num">2.</span>
							<h2>{locationObj.mainPage.typesOfTours.items[1].title}</h2>
							<p>{locationObj.mainPage.typesOfTours.items[1].description}</p>
							<Link to={`${lang.lang}/ground_services`}>More details</Link>
						</div>
						<div className="img"></div>
					</div>
					<div className="type" id="private_and_organizated_tours">
						<div className="description">
							<span className="num">3.</span>
							<h2>{locationObj.mainPage.typesOfTours.items[2].title}</h2>
							<p>{locationObj.mainPage.typesOfTours.items[2].description}</p>
							<Link to={`${lang.lang}/private_and_organizated_tours`}>More details</Link>
						</div>
						<div className="img"></div>
					</div>
				</div>
				<div className="tours" id="most_popular">
					<h1>{locationObj.mainPage.mostPopularTours.title}</h1>
					<div className="divider"></div>
					<div className="desktop-line"></div>
					<div className="cards">
						{this._renderTours.bind(this)()}
					</div>
					<Paginator isTourCards={true} count={this.props.tours.length} switch={this._switchTour.bind(this)} />
					<div className="description" id="about_us">
						<h1>{locationObj.mainPage.aboutUs.title}</h1>
						<p>{locationObj.mainPage.aboutUs.descriptionDesktop}</p>
					</div>
				</div>
				<div className="reviews" id="reviews">
					<div className="some-things"></div>
					<div className="review-cards">
						{this._renderComments.bind(this)()}
					</div>
					<Paginator isTourCards={false} count={this.props.comments.length} switch={this._switchComment.bind(this)} />
				</div>
				<div className="test" >
					<div className="question">
						<Test 
							saveTestResults={this.props.saveTestResults} 
							submitEmail={this.props.submitEmail}
						/>
					</div>
				</div>
				<footer>
                    <div className="ss-links">
                        <a className="instagram" href="https://www.instagram.com/blue_mountains_tours__/"></a>
                        <a className="linkedin" href="https://www.linkedin.com/in/menashe-salomon-a262015b/?challengeId=AQG-W2BBTc6jiAAAAXMfIBgsanfx3DE5t5e8TtcyK7wcBz_2pKs1zEUlP60ysC28I0YDKkopRbTbRwqxFYv0HSH3VrlBqw-quA&submissionId=0b2d1266-afdd-1e16-a263-3583750c4d33"></a>
                        <a className="facebook mob" href="fb://www.facebook.com/zanzatours/"></a>
						<a className="facebook desk" href="https://www.facebook.com/zanzatours/"></a>
                    </div>
					© 2020 zanzatours.com
				</footer>
			</div>
		)
	}
}


const mapStateToProps = (state: AppState) => ({
	tours: state.landing.tours,
	comments: state.landing.comments
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	getTours: async () => {
		dispatch(getTours());
	},
	getComments: async (isMainPage:boolean) => {
		dispatch(getComments(isMainPage));
	},
	saveTestResults: async (results:string) => {
		dispatch(saveTestResults(results));
	},
	submitEmail: async (
		email:string, test_results:string, cb: (res:any) => void
	) => {
		dispatch(
			submitEmail(
				email, test_results, cb
			)
		);
	}
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));