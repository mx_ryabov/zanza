import React, { Component } from 'react'
import ILocalization from '../localization/interface';
import Header from '../components/Header';
import AliceCarousel from 'react-alice-carousel';
import he from '../localization/he';
import en from '../localization/en';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { IComment } from '../store/landing/types';
import CommentCard from '../components/CommentCard';
import Paginator from '../components/Paginator';
import { AppState } from '../store';
import { ThunkDispatch } from 'redux-thunk';
import { getComments } from '../store/landing/actions';
import { connect } from 'react-redux';

interface props extends RouteComponentProps {
    comments: IComment[],
	getComments: (isMainPage:boolean) => Promise<void>;
}

interface state {
    currentCommentInd: number;
}

class AboutUsPage extends Component<props, state> {
    state = {
        currentCommentInd: 0
    }

	componentDidMount() {
		this.props.getComments(false);
	}

	_renderLandscapes = () => {
		return ["first", "second", "third"].map((land_item:string, ind:number) => {
			return (<div className={`landscape ${land_item}`} key={ind}></div>)
		})
    }
    
	_switchComment(indexComment: number) {
		this.setState({currentCommentInd: indexComment});
	}

	_renderComments() {
		return this.props.comments.map((comment:IComment, ind:number) => {
			return (
				<CommentCard 
					key={ind} 
					title={comment.title}
					text={comment.text}
					rating={comment.rating}
					addressant={comment.addressant}
					date={comment.date.split('-').join('.')}
					style={{transform: `translate(-${this.state.currentCommentInd*100}%)`}}
				/>
			)
		})
	}
    

    render() {
		const lang:any = this.props.match.params;
		const locationObj:ILocalization = lang.lang === "he" ? he : en;	
        return (
            <div className={`about-us-page ${lang.lang === "he" ? "he" : "en"}`}>
                <Header />
                <div className="about-us" id="about_us">
                    <AliceCarousel 
                        mouseTrackingEnabled
                        dotsDisabled={true}
                        buttonsDisabled={true}
						autoPlay={true}
						autoPlayInterval={5000}
                        responsive={{0: {items: 1}}}
                    >
                        {this._renderLandscapes()}
                    </AliceCarousel>
                    <div className="description">
                        <h1>{locationObj.mainPage.aboutUs.title}</h1>
                        <p className="desk">
                            {locationObj.mainPage.aboutUs.descriptionDesktop}
                        </p>
                        <p className="mob">
                            {locationObj.mainPage.aboutUs.descriptionMobile}
                        </p>
                    </div>
                    <a className="phone_number" href="tel:+61430049139">
                        +61 430 049139
                    </a>
                </div>
                <div className="reviews" id="reviews">
					<div className="some-things"></div>
					<div className="review-cards">
						{this._renderComments.bind(this)()}
					</div>
                    <Paginator 
                        isTourCards={false} 
                        count={this.props.comments.length} 
                        switch={this._switchComment.bind(this)} 
                    />
				</div>
				<footer>
                    <div className="ss-links">
                        <a className="instagram" href="https://www.instagram.com/blue_mountains_tours__/"></a>
                        <a className="linkedin" href="https://www.linkedin.com/in/menashe-salomon-a262015b/?challengeId=AQG-W2BBTc6jiAAAAXMfIBgsanfx3DE5t5e8TtcyK7wcBz_2pKs1zEUlP60ysC28I0YDKkopRbTbRwqxFYv0HSH3VrlBqw-quA&submissionId=0b2d1266-afdd-1e16-a263-3583750c4d33"></a>
                        <a className="facebook" href="https://www.facebook.com/zanzatours/"></a>
                    </div>
					© 2020 zanzatours.com
				</footer>
            </div>
        )
    }
}


const mapStateToProps = (state: AppState) => ({
	comments: state.landing.comments
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	getComments: async (isMainPage:boolean) => {
		dispatch(getComments(isMainPage));
	}
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutUsPage));
