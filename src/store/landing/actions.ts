import {
    START_FETCH, LandingActionTypes, StartFetchAction, 
    GET_COMMENTS, FETCH_ERROR, GET_TOURS, SUBMIT_DETAILS, SAVE_TEST_RESULTS, GET_GALLERY, 
} from './types';
import { getRequest, postRequest } from "../../helpers";
import { AnyAction } from "redux";
import { ThunkAction, ThunkDispatch } from "redux-thunk";



export const saveTestResults = (results:string) => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
    dispatch({ type: SAVE_TEST_RESULTS, payload: results });
}


export const getGallery = ():ThunkAction<Promise<void>, {}, {}, AnyAction> => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);    
    
    try {
        const res:any = await getRequest("/gallery/get", {});
        
        const success:LandingActionTypes = {type: GET_GALLERY, payload: res.res};
        dispatch(success);
    } catch(e) {
        const error:LandingActionTypes = {type: FETCH_ERROR, payload: e };
        dispatch(error);
    } 
}

export const submitDetails = (
        isAgree:boolean, name:string, 
        phone_number:string, email:string, test_results:string,
        cb: (res:any) => void
    ):ThunkAction<Promise<void>, {}, {}, AnyAction> => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);   
    
    try {
        const res:any = await postRequest("/details/submit", {isAgree, name, phone_number, email, test_results});
        
        const success:LandingActionTypes = {type: SUBMIT_DETAILS, payload: res};
        dispatch(success);
        cb(res);
    } catch(e) {
        const error:LandingActionTypes = {type: FETCH_ERROR, payload: e };
        dispatch(error);
    } 
}

export const submitEmail = (
    email:string, test_results:string,
    cb: (res:any) => void
):ThunkAction<Promise<void>, {}, {}, AnyAction> => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
const fetching:StartFetchAction = {type: START_FETCH};
dispatch(fetching);   

try {
    const res:any = await postRequest("/email/submit", { email, test_results });
    
    const success:LandingActionTypes = {type: SUBMIT_DETAILS, payload: res};
    dispatch(success);
    cb(res);
} catch(e) {
    const error:LandingActionTypes = {type: FETCH_ERROR, payload: e };
    dispatch(error);
} 
}


export const getTours = (type?: "au" | "nz"):ThunkAction<Promise<void>, {}, {}, AnyAction> => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);    
    
    try {
        const res:any = await getRequest("/tours/get", type? {type}: {});
        
        const success:LandingActionTypes = {type: GET_TOURS, payload: res.res};
        dispatch(success);
    } catch(e) {
        const error:LandingActionTypes = {type: FETCH_ERROR, payload: e };
        dispatch(error);
    } 
}


export const getComments = (isMainPage:boolean):ThunkAction<Promise<void>, {}, {}, AnyAction> => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);    
    
    try {
        const res:any = await getRequest("/comments/get", {is_main_page: isMainPage? "True" : "False"});
        
        const success:LandingActionTypes = {type: GET_COMMENTS, payload: res.res};
        dispatch(success);
    } catch(e) {
        const error:LandingActionTypes = {type: FETCH_ERROR, payload: e };
        dispatch(error);
    } 
}