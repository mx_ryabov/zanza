import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import configureStore from './store';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import Switcher from './Switcher';
import ReactGA from 'react-ga';
import {createBrowserHistory} from 'history';
import ScrollToTop from './components/ScrollToTop';

ReactGA.initialize('UA-161289106-1');

const history = createBrowserHistory();

ReactGA.set({ page: window.location.pathname });
ReactGA.pageview(window.location.pathname);

history.listen((location, action) => {
    ReactGA.set({ page: location.pathname });
    ReactGA.pageview(location.pathname);
});

const store = configureStore();


ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <ScrollToTop />
            <Switcher />
        </Router>
    </Provider>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
